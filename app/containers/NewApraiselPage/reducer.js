/*
 *
 * NewApraiselPage reducer
 *
 */

import { fromJS } from 'immutable';

import {
  LOAD_CATEGORIES,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_CATEGORIES_FAILURE,
} from './constants';

const initialState = fromJS({
   loading: false,
   error: false,
   categories: false,
   users: false,
 });

function newApraiselPageReducer(state = initialState, action) {
  switch (action.type) {
    case LOAD_CATEGORIES:
      return state
        .set('creating', true);
    case LOAD_CATEGORIES_SUCCESS:
      return state
        .set('creating', false)
        .set('categories', action.response.categories)
        .set('users', action.response.users)
    case LOAD_CATEGORIES_FAILURE:
      return state
        .set('error', action.error)
        .set('creating', false);
    default:
      return state;
  }
}

export default newApraiselPageReducer;
