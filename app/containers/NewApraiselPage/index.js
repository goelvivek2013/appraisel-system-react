/**
 *
 * NewApraiselPage
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Field, reduxForm } from 'redux-form/immutable';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import { makeSelectLoadCategories, makeSelectError, makeSelectLoading } from './selectors';

import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import {loadCategories} from './actions';

import AppraiseUser from '../AppraiseUser/index'

import AppraiseUserList from 'components/AppraiseUserList';


export class NewApraiselPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => {
    return(
      <TextField
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
      />
    )
  }
  componentDidMount() {
    console.log('here')
    var x = this.props.loadCategories();
    console.log('categories', x)
  }

  onSubmit(values) {
    console.log('values', values);
    this.props.newAppraisel(values, () => {
      this.props.history.push("/")
    })
  }
  render() {
    const { handleSubmit } = this.props;
    const { loading, error, categories } = this.props;
    const appraiseUserListProps = {
      loading: loading,
      error: error,
      users: categories.users,
      categories: categories.categories,
    }
    console.log("appraiseUserListProps", this.props);
    console.log("appraiseUserListProps", appraiseUserListProps);
    return(
      <div>
        <AppraiseUserList {...appraiseUserListProps} />
      </div>
    )
  }
}

const validate = (values) => {
  const errors = {}
  const requiredFields = [
    'comment'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'required'
    }
  })
  return errors
}

NewApraiselPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
  categories: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.bool,
  ]),

};

const mapStateToProps = createStructuredSelector({
  categories: makeSelectLoadCategories(),
  loading: makeSelectLoading(),
  error: makeSelectError(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(mapStateToProps, {loadCategories});

const withReducer = injectReducer({ key: 'newApraiselPage', reducer});
const withSaga = injectSaga({ key: 'newApraiselPage', saga });
const withForm = reduxForm({ form: 'newApraiselPage', enableReinitialize : true, validate });

export default compose(
  withForm,
  withReducer,
  withSaga,
  withConnect,
)(NewApraiselPage);
