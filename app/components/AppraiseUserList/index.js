import React from 'react';
import PropTypes from 'prop-types';

import List from 'components/List';
import ListItem from 'components/ListItem';
import LoadingIndicator from 'components/LoadingIndicator';
import AppraiseUser from 'containers/AppraiseUser';

function AppraiseUserList({ loading, error, users, categories }) {
  if (loading) {
    return <List component={LoadingIndicator} />;
  }

  if (error !== false) {
    const ErrorComponent = () => (
      <ListItem item={'Something went wrong, please try again!'} />
    );
    return <List component={ErrorComponent} />;
  }

  if (categories !== false) {
    return <List items={users} categories = {categories} component={AppraiseUser} />;
  }

  return null;
}

AppraiseUserList.propTypes = {
  loading: PropTypes.bool,
    error: PropTypes.any,
    repos: PropTypes.any,
};

export default AppraiseUserList;
