// import { take, call, put, select } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';

import { SIGNUP_USER } from './constants';
import { signUpUserSuccess, signUpUserError } from './actions';

export function* signupUser({user}) {
  const requestURL = 'http://localhost:3001/auth';

  console.log('post in addPost', user);
  console.log('post in addPost', JSON.stringify(user));
  try {
    // Call our request helper (see 'utils/request')
    const created = yield call(request, requestURL, {
      method: 'POST',
      body: JSON.stringify(user),
      withCredentials: true,
      mode: "cors",
      headers : {
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json'
      }
    });
    yield put(signUpUserSuccess(created));
  } catch (err) {
    yield put(signUpUserError(err));
  }
}

export default function* signUpUser() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(SIGNUP_USER, signupUser);
}
