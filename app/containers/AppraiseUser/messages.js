/*
 * AppraiseUser Messages
 *
 * This contains all the text for the AppraiseUser component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.AppraiseUser.header',
    defaultMessage: 'This is AppraiseUser container !',
  },
});
