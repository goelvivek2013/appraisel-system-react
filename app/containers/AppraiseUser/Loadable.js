/**
 *
 * Asynchronously loads the component for AppraiseUser
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
