/*
 * LoadCategories Messages
 *
 * This contains all the text for the LoadCategories component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.LoadCategories.header',
    defaultMessage: 'This is LoadCategories container !',
  },
});
