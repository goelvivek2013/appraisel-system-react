/**
 *
 * Asynchronously loads the component for NewApraiselPage
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
