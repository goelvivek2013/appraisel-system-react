/*
 *
 * AppraiseUser constants
 *
 */

export const APPRAISE_USER = 'app/AppraiseUser/APPRAISE_USER';
export const APPRAISE_USER_SUCCESS = 'app/AppraiseUser/APPRAISE_USER_SUCCESS';
export const APPRAISE_USER_FAILURE = 'app/AppraiseUser/APPRAISE_USER_FAILURE';
