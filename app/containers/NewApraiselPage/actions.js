/*
 *
 * NewApraiselPage actions
 *
 */

import {
  LOAD_CATEGORIES,
  LOAD_CATEGORIES_SUCCESS,
  LOAD_CATEGORIES_FAILURE,
} from './constants';

export function loadCategories() {
  return {
    type: LOAD_CATEGORIES,
  };
}
export function loadCategoriesSuccess(response) {
  console.log(response)
  return {
    type: LOAD_CATEGORIES_SUCCESS,
    response,
  };
}
export function loadCategoriesFailure(error) {
  return {
    type: LOAD_CATEGORIES_FAILURE,
    error
  };
}
