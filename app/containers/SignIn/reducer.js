/*
 *
 * SignIn reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNIN_USER_FAILURE,
} from './constants';

const initialState = fromJS({});

function signInReducer(state = initialState, action) {
  switch (action.type) {
    case SIGNIN_USER:
      return state
        .set('creating', true);
    case SIGNIN_USER_SUCCESS:
      return state
        .set('creating', false)
        .set('created', true);
    case SIGNIN_USER_FAILURE:
      return state
        .set('error', action.error)
        .set('creating', false);
    default:
      return state;
  }
}

export default signInReducer;
