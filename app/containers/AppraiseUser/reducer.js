/*
 *
 * AppraiseUser reducer
 *
 */

import { fromJS } from 'immutable';
import {
  APPRAISE_USER,
  APPRAISE_USER_SUCCESS,
  APPRAISE_USER_FAILURE,
} from './constants';

const initialState = fromJS({});

function appraiseUserReducer(state = initialState, action) {
  switch (action.type) {
    case APPRAISE_USER:
      return state;
    case APPRAISE_USER_SUCCESS:
      return state;
    case APPRAISE_USER_FAILURE:
      return state;
    default:
      return state;
  }
}

export default appraiseUserReducer;
