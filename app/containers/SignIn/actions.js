/*
 *
 * SignIn actions
 *
 */

import {
  SIGNIN_USER,
  SIGNIN_USER_SUCCESS,
  SIGNIN_USER_FAILURE,
} from './constants';

export function signInUser(user) {
  return {
    type: SIGNIN_USER,
    user
  };
}

export function signInUserSuccess(response) {
  console.log(response)
  return {
    type: SIGNIN_USER_SUCCESS,
  };
}

export function signInUserError(error) {
  return {
    type: SIGNIN_USER_FAILURE,
    error,
  };
}
