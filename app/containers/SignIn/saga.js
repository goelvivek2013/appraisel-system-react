// import { take, call, put, select } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';

import { SIGNIN_USER } from './constants';
import { signInUserSuccess, signInUserError } from './actions';

export function* signInUser({user}) {
  const requestURL = 'http://localhost:3001/auth/sign_in';

  console.log('post in addPost', user);
  console.log('post in addPost', JSON.stringify(user));
  try {
    // Call our request helper (see 'utils/request')
    const created = yield call(request, requestURL, {
      method: 'POST',
      body: JSON.stringify(user),
      withCredentials: true,
      mode: "cors",
      headers : {
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json',
        "Access-Control-Allow-Credentials": true,
        'access-token': '',
        'token-type': '',
        'client': '',
        'expiry': '',
        'uid': ''
      }
    });
    yield put(signInUserSuccess(created));
  } catch (err) {
    yield put(signInUserError(err));
  }
}

export default function* signinUser() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(SIGNIN_USER, signInUser);
}
