/**
 *
 * AppraiseUser
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Field, reduxForm, FieldArray} from 'redux-form/immutable';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectAppraiseUser from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { appraiseUser } from './actions';

export class AppraiseUser extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => {
    return(
      <TextField
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
      />
    )
  }
  onSubmit(values) {
    console.log('values', values);
    this.props.appraiseUser(values, () => {
      this.props.history.push("/")
    })
  }

  renderCategoryForm = ({ fields, meta: { touched, error } }) => (
    <ul>
      {fields.map((category, index) =>
        <li key={index}>
          <br />
          <Field
            name="comment"
            label="Comment"
            component={this.renderTextField}
          />
          <br />
          <Field
            name="points"
            label="Points"
            component={this.renderTextField}
          />
          <br />
          <input
            type='hidden'
            name="category_id"
            value={category.id}
          />
        </li>
      )}
      {error && <li className="error">{error}</li>}
    </ul>
  )


  render() {
    console.log('appraise user props', this.props)
    const { item, categories } = this.props;
    const { handleSubmit } = this.props
    const content = categories.map((category) => (
      <div key={`category-${category.id}`}>
      {category.body}
      <FieldArray name={`${item}.categories`} component={this.renderCategoryForm}/>
      </div>
    ));
    console.log('appraiseUserListProps', this.props)
    return (
      <div>
        {item.email}
        <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) } key={`form-${item.id}`}>
          {content}
          <RaisedButton
            primary={true}
            label='Appraise'
            type="submit" />

          <RaisedButton
            label='Cancel'
            style={{marginLeft: '10px'}}
            onClick={this.cancel} />
        </form>
      </div>
    );
  }
}

const validate = (values) => {
  const errors = {}
  const requiredFields = [
    'comment',
    'points'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'required'
    }
  })
  return errors
}

AppraiseUser.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  appraiseuser: makeSelectAppraiseUser(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, {appraiseUser});

const withReducer = injectReducer({ key: 'appraiseUser', reducer });
const withSaga = injectSaga({ key: 'appraiseUser', saga });
const withForm = reduxForm({ form: 'appraiseUser', enableReinitialize : true, validate });

export default compose(
  withForm,
  withReducer,
  withSaga,
  withConnect,
)(AppraiseUser);
