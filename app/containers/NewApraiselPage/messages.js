/*
 * NewApraiselPage Messages
 *
 * This contains all the text for the NewApraiselPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.NewApraiselPage.header',
    defaultMessage: 'This is NewApraiselPage container !',
  },
});
