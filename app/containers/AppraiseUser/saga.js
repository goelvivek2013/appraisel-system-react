// import { take, call, put, select } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';

import { reactLocalStorage } from 'reactjs-localStorage';

import { APPRAISE_USER } from './constants';
import { appraiseUserSucess, appraiseUserFailure } from './actions';

export function* appraiseUser(appraisel) {
  const requestURL = 'http://localhost:3001/appraisels';

  console.log('post in appraise',apprasie);
  console.log('post in appraise', appraise);
  try {
    // Call our request helper (see 'utils/request')
    const created = yield call(request, requestURL, {
      method: 'POST',
      withCredentials: true,
      mode: "cors",
      body: JSON.stringify(appraisel),
      headers : {
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json',
        'access-token': reactLocalStorage.get('access-token'),
        'token-type': reactLocalStorage.get('token-type'),
        'client': reactLocalStorage.get('client'),
        'expiry': reactLocalStorage.get('expiry'),
        'uid': reactLocalStorage.get('uid')
      }
    });
    yield put(appraiseUserSuccess(created));
  } catch (err) {
    yield put(appraiseUserFailure(err));
  }
}

export default function* appraiseuser() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(APPRAISE_USER, appraiseUser);
}
