/*
 *
 * SignIn constants
 *
 */

export const SIGNIN_USER = 'app/SignIn/SIGNIN_USER';
export const SIGNIN_USER_SUCCESS = 'app/SignIn/SIGNIN_USER_SUCCESS';
export const SIGNIN_USER_FAILURE = 'app/SignIn/SIGNIN_USER_FAILURE';
