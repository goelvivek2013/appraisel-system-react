import { createSelector } from 'reselect';

/**
 * Direct selector to the appraiseUser state domain
 */
const selectAppraiseUserDomain = (state) => state.get('appraiseUser');

/**
 * Other specific selectors
 */


/**
 * Default selector used by AppraiseUser
 */

const makeSelectAppraiseUser = () => createSelector(
  selectAppraiseUserDomain,
  (substate) => substate.toJS()
);

export default makeSelectAppraiseUser;
export {
  selectAppraiseUserDomain,
};
