/*
 *
 * LoadCategories constants
 *
 */

 export const LOAD_CATEGORIES = 'app/NewApraiselPage/LOAD_CATEGORIES';
 export const LOAD_CATEGORIES_SUCCESS = 'app/NewApraiselPage/LOAD_CATEGORIES_SUCCESS';
 export const LOAD_CATEGORIES_FAILURE = 'app/NewApraiselPage/LOAD_CATEGORIES_FAILURE';
