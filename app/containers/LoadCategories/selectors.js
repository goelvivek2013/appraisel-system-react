import { createSelector } from 'reselect';

/**
 * Direct selector to the loadCategories state domain
 */
const selectLoadCategoriesDomain = (state) => state.get('loadCategories');

/**
 * Other specific selectors
 */


/**
 * Default selector used by LoadCategories
 */

const makeSelectLoadCategories = () => createSelector(
  selectLoadCategoriesDomain,
  (globalState) => globalState.toJS('loadCategories')
);

const makeSelectLoading = () => createSelector(
  selectLoadCategoriesDomain,
  (globalState) => globalState.get('loading')
);

const makeSelectError = () => createSelector(
  selectLoadCategoriesDomain,
  (globalState) => globalState.get('error')
);

export default makeSelectLoadCategories;
export {
  selectLoadCategoriesDomain,
  makeSelectLoading,
  makeSelectError,
  makeSelectLoadCategories
};
