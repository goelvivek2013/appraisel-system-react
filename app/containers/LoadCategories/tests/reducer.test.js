
import { fromJS } from 'immutable';
import loadCategoriesReducer from '../reducer';

describe('loadCategoriesReducer', () => {
  it('returns the initial state', () => {
    expect(loadCategoriesReducer(undefined, {})).toEqual(fromJS({}));
  });
});
