/*
 *
 * SignUp reducer
 *
 */

import { fromJS } from 'immutable';
import {
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,
} from './constants';

const initialState = fromJS({});

function signUpReducer(state = initialState, action) {
  switch (action.type) {
    case SIGNUP_USER:
      return state
        .set('creating', true);
    case SIGNUP_USER_SUCCESS:
      return state
        .set('creating', false)
        .set('created', true);
    case SIGNUP_USER_FAILURE:
      return state
        .set('error', action.error)
        .set('creating', false);
    default:
      return state;
  }
}

export default signUpReducer;
