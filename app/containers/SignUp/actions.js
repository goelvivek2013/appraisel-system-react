/*
 *
 * SignUp actions
 *
 */

import {
  SIGNUP_USER,
  SIGNUP_USER_SUCCESS,
  SIGNUP_USER_FAILURE,
} from './constants';

export function signUpUser(user) {
  return {
    type: SIGNUP_USER,
    user
  };
}

export function signUpUserSuccess(response) {
  console.log(response)
  return {
    type: SIGNUP_USER_SUCCESS,
  };
}

export function signUpUserError(error) {
  return {
    type: SIGNUP_USER_FAILURE,
    error,
  };
}
