/*
 *
 * SignUp constants
 *
 */

export const SIGNUP_USER = 'app/SignUp/SIGNUP_USER';
export const SIGNUP_USER_SUCCESS = 'app/SignUp/SIGNUP_USER_SUCCESS';
export const SIGNUP_USER_FAILURE = 'app/SignUp/SIGNUP_USER_FAILURE';
