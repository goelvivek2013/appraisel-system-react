/*
 *
 * AppraiseUser actions
 *
 */

import {
  APPRAISE_USER,
  APPRAISE_USER_SUCCESS,
  APPRAISE_USER_FAILURE,
} from './constants';

export function appraiseUser(appraisel) {
  console.log('appraisel', appraisel)
  return {
    type: APPRAISE_USER,
    appraisel,
  };
}
export function appraiseUserSucess(apprasie) {
  return {
    type: APPRAISE_USER_SUCCESS,
    apprasie
  };
}
export function appraiseUserFailure(error) {
  return {
    type: APPRAISE_USER_FAILURE,
    error
  };
}
