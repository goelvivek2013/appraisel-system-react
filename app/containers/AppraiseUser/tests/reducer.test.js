
import { fromJS } from 'immutable';
import appraiseUserReducer from '../reducer';

describe('appraiseUserReducer', () => {
  it('returns the initial state', () => {
    expect(appraiseUserReducer(undefined, {})).toEqual(fromJS({}));
  });
});
