
import { fromJS } from 'immutable';
import newApraiselPageReducer from '../reducer';

describe('newApraiselPageReducer', () => {
  it('returns the initial state', () => {
    expect(newApraiselPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
