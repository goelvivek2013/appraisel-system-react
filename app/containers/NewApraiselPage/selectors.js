import { createSelector } from 'reselect';

/**
 * Direct selector to the newApraiselPage state domain
 */
const selectNewApraiselPageDomain = (state) => state.get('newApraiselPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by NewApraiselPage
 */

 const makeSelectLoadCategories = () => createSelector(
   selectNewApraiselPageDomain,
   (globalState) => globalState.toJS('newApraiselPage')
 );

 const makeSelectLoading = () => createSelector(
   selectNewApraiselPageDomain,
   (globalState) => globalState.get('loading')
 );

 const makeSelectError = () => createSelector(
   selectNewApraiselPageDomain,
   (globalState) => globalState.get('error')
 );

 export default makeSelectLoadCategories;
 export {
   selectLoadCategoriesDomain,
   makeSelectLoading,
   makeSelectError,
   makeSelectLoadCategories
 };
