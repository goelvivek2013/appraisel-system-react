// import { take, call, put, select } from 'redux-saga/effects';
import { call, put, takeEvery } from 'redux-saga/effects';
import request from 'utils/request';

import { reactLocalStorage } from 'reactjs-localStorage';

import { LOAD_CATEGORIES } from './constants';
import { loadCategoriesSuccess, loadCategoriesFailure } from './actions';

export function* loadCategories() {
  const requestURL = 'http://localhost:3001/categories';

  console.log('post in addPost',);
  console.log('post in addPost');
  try {
    // Call our request helper (see 'utils/request')
    const created = yield call(request, requestURL, {
      method: 'GET',
      withCredentials: true,
      mode: "cors",
      headers : {
        'Accept'        : 'application/json',
        'Content-Type'  : 'application/json',
        'access-token': reactLocalStorage.get('access-token'),
        'token-type': reactLocalStorage.get('token-type'),
        'client': reactLocalStorage.get('client'),
        'expiry': reactLocalStorage.get('expiry'),
        'uid': reactLocalStorage.get('uid')
      }
    });
    yield put(loadCategoriesSuccess(created));
  } catch (err) {
    yield put(loadCategoriesFailure(err));
  }
}

export default function* loadcategories() {
  // See example in containers/HomePage/saga.js
  yield takeEvery(LOAD_CATEGORIES, loadCategories);
}
