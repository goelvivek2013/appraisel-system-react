/**
 *
 * SignUp
 *
 */

import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { compose } from 'redux';

import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import { Field, reduxForm } from 'redux-form/immutable';

import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import makeSelectSignUp from './selectors';
import reducer from './reducer';
import saga from './saga';
import messages from './messages';
import { signUpUser } from './actions'

export class SignUp extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  renderTextField = ({ input, label, meta: { touched, error }, ...custom }) => {
    return(
      <TextField
        floatingLabelText={label}
        errorText={touched && error}
        {...input}
        {...custom}
      />
    )
  }

  onSubmit(values) {
    console.log('values', values);
    this.props.signUpUser(values, () => {
      this.props.history.push("/")
    })
  }
  render() {
    const { handleSubmit } = this.props
    console.log("this.props", this.props);
    return(
      <div>
        <form onSubmit={ handleSubmit(this.onSubmit.bind(this)) }>
          <Field
            name="email"
            label="Email"
            component={this.renderTextField}
          />
          <br />
          <Field
            name="password"
            label="Password"
            type="password"
            component={this.renderTextField}
          />
          <br />
          <Field
            name="password_confirmation"
            label="Password"
            component={this.renderTextField}
          />
          <br />
          <RaisedButton
            primary={true}
            label='Sign Up'
            type="submit" />

          <RaisedButton
            label='Cancel'
            style={{marginLeft: '10px'}}
            onClick={this.cancel} />
        </form>
      </div>
    )
  }
}

const validate = (values) => {
  const errors = {}
  const requiredFields = [
    'email',
    'password',
    'password_confirmation'
  ]
  requiredFields.forEach(field => {
    if (!values[field]) {
      errors[field] = 'required'
    }
  })
  return errors
}

SignUp.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  signup: makeSelectSignUp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

const withConnect = connect(null, {signUpUser});

const withReducer = injectReducer({ key: 'signUp', reducer });
const withSaga = injectSaga({ key: 'signUp', saga });
const withForm = reduxForm({ form: 'signUp', enableReinitialize : true, validate });

export default compose(
  withForm,
  withReducer,
  withSaga,
  withConnect,
)(SignUp);
